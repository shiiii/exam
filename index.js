/**
 * @format
 */

 import React from 'react'

 import { Provider } from "react-redux";
 import configureStore from './app/store/configureStore';
 import { PersistGate } from "redux-persist/lib/integration/react";
 import { persistStore } from 'redux-persist'
 import {AppRegistry} from 'react-native';
 import App from './App';
 import {name as appName} from './app.json';

 const store = configureStore()
 const persist = persistStore(store)
 
 const app = () => (
     <Provider store={store}>
        <PersistGate loading={null} persistor={persist}>
            <App />
        </PersistGate>
     </Provider>
 )
 

AppRegistry.registerComponent(appName, () => app);
