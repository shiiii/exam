
export const colors = {
  babyPowder: "#b8eff4",
  babyBlue: "#6edbe6",
  primary: "#325d6e",
  secondary: "#456d7c",
  tertiary: "#7a97a2",

  gray: "#819da7",
  white: '#F5F5F5',
  black: "#333333",

  primaryFont: "#ffffff",
  secondaryFont: "#2f3840",

  background: "#325d6e",
  secondaryBackground: "#ffffff",

}

