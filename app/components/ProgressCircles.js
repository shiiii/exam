import React from 'react'
import ProgressCircle from 'react-native-progress-circle'

import { colors } from '../config/colors'


const AppProgressCircle = ({children, value}) => {
        return (
            <ProgressCircle
                percent={value}
                radius={110}
                borderWidth={5}
                color= {colors.white}
                shadowColor={colors.secondary}
                bgColor= {colors.secondary}
            >
                {children}
            </ProgressCircle>
        )
}

export default AppProgressCircle