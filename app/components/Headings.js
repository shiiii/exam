import React from 'react'
import {StatusBar, View} from 'react-native'
import Text from './Text'

import { colors } from '../config/colors'

const Headings = ({header, subHeader}) => {
    return (
        <View style = {{ marginVertical: 10,}}>
            <Text bold  style ={{fontSize: 20, color : colors.black }}>{header}</Text>
            <Text bold  style ={{fontSize: 14, color : colors.black }}>{subHeader}</Text>
        </View>
    )
}

export default  Headings