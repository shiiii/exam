import React from "react"
import { TouchableOpacity, StyleSheet } from "react-native"

import { colors } from "../config/colors"
import Text from "./Text"

const Button = ({ children, onPress, style }) => {
  return (
      <TouchableOpacity style={[styles.button, style]} onPress={() => onPress()}>
        <Text bold style={styles.label}>
          {children}
        </Text>
      </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.white,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  label: {
    fontSize: 14,
    color: colors.black,
  },
})

export default Button
