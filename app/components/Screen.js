import React from 'react'
import { SafeAreaView, StyleSheet, View } from 'react-native'
import { colors } from '../config/colors'

function Screen({ children, style }) {
  return (
    <SafeAreaView style={[styles.screen, style]}>
      <View style={[styles.view, style]}>{children}</View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: colors.white
  },
  view: {
    flex: 1
  }
})

export default Screen
