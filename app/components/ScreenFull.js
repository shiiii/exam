import React from 'react'
import { StyleSheet, View } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context'


function ScreenFull({ children, style }) {
  const insets = useSafeAreaInsets()

  return (
    <View style={[styles.screen, style, { marginTop: insets.top }]}>
      {children}
    </View>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F5F5F5'
  },
  view: {
    flex: 1
  }
})

export default ScreenFull
