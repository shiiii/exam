import React, {useState} from 'react'
import {View, StyleSheet,} from 'react-native'
import { colors } from '../config/colors'
import Space from './Space'


const StepIndicator = ({ numberOfStep, current}) => {

    const renderStep = () =>{
        let stepIndicator = []
        for (let step = 0; step < numberOfStep; step++)  {
            stepIndicator.push(
                <>
                    <View key= {step} style ={ [styles.step, { backgroundColor:  step <= current - 1 ? colors.white : colors.gray }]} />
                    {step < (numberOfStep - 1) && <Space width={10}/>}
                </>
            )
        }
        return stepIndicator
    }

    return (
        <View style= {[styles.container,]}  >
            <View style ={{ flexDirection: 'row' }}>
              {renderStep()}
            </View>
        </View>
    )
}

export default StepIndicator

const styles = StyleSheet.create({
    container:  {
        zIndex: 1, 
        position: 'absolute', 
        left: 0, 
        right:0, 
        top: 35,
        marginHorizontal: 15, 
        alignItems: 'center',
    },
    step: {
        height: 4,
        flex:1,
        borderRadius: 2,
    }

})
