import React, {useState} from 'react'
import {View, StyleSheet,} from 'react-native'
import { useNavigation } from '@react-navigation/native'

import Text from './Text'
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import { colors } from '../config/colors'
import Button from './Button'
import Space from './Space'


const Header = ({children, style}) => {
    const navigation = useNavigation()
    return (
        <View style= {[styles.container, style]}  >
            <Space height={50}></Space>
            <View style= {styles.headerContianer}>
                <Text style={styles.headerText}>{children}</Text>
                <Button style = {styles.button} onPress = {() =>  navigation.goBack(null)}>
                    <Icon name="close" size={30} style={styles.icon} />
                </Button>
            </View>
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    container:  {
        zIndex: 1, 
        position: 'absolute', 
        left: 0, 
        right:0, 
        marginHorizontal: 15, 
        alignItems: 'center'
    },
    headerContianer: { 
        flexDirection: 'row',  
        justifyContent: 'space-between', 
        alignItems: 'center' 
    },
    headerText: {
        flex:1,
        fontSize: 12
    },
    button: { 
        backgroundColor: 'transparent'
    },
    icon: {
        fontSize: 20,
        color: colors.white,
        paddingHorizontal: 15,
        paddingVertical: 10
    },
})
