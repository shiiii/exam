import React from "react"
import { Text } from "react-native"
import { RFValue } from "react-native-responsive-fontsize"
import { colors } from "../config/colors"

const AppText = ({ bold = false, style, fontSize = 12, children, ...props }) => {
  return (
    <Text
      maxFontSizeMultiplier={1.15}
      style={[{
        fontSize: RFValue(fontSize, 812),
        fontFamily: bold ? "Montserrat-Bold" : "Montserrat-Regular",
        color: colors.primaryFont,
      }, style]} {...props}>
      {children}
    </Text>
  )
}

export default AppText