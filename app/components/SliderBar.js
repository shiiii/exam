import React from 'react'
import {View, StyleSheet} from 'react-native'
import Slider from 'react-native-custom-slider'
import { colors } from '../config/colors'


const SliderBar = ({onValueChange, style}) => {

    return (
        <View style = {[styles.container, style]}>
            <Slider
                style ={{ marginHorizontal: 30 }}
                minimumValue={0}
                maximumValue={1}
                minimumTrackTintColor={colors.babyBlue}
                maximumTrackTintColor={colors.white}
                thumbStyle={{ borderWidth: 2, borderColor: colors.white, backgroundColor: colors.babyBlue, width: 20, height: 20, }}
                trackStyle ={{height: 15, borderRadius: 8, }}
                onValueChange={value => onValueChange(value)}
                >
            </Slider>
        </View>
    )
      
}

export default SliderBar

const styles = StyleSheet.create({
    container: {
        marginTop: 50,
      
    }
})
