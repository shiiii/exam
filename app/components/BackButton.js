import React from 'react'
import { StyleSheet, Image, TouchableOpacity } from "react-native"
import { useNavigation } from '@react-navigation/native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'


function BackButton() {
  const navigation = useNavigation()

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => navigation.goBack(null)}
      style={styles.headerBackTouch}>
      <Icon name="back" size={30} style={styles.icon} />

    </TouchableOpacity>
  )
}

export default BackButton

const styles = StyleSheet.create({
  headerBackTouch: {
    width: 38,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerBackImage: {
    tintColor: '#000',
    right: 3.5,
    width: 21,
    height: 21,
    resizeMode: 'contain',
  },
})