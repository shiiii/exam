import React from "react"
import { View } from "react-native"

const Space = ({ width = 10, height = 10 }) => {
  return <View style={{ width: width, height: height }}></View>
}
export default Space