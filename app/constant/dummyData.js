import low from '../assets/images/pyramid/low.png'
import fine from '../assets/images/pyramid/fine.png'
import moderate from '../assets/images/pyramid/moderate.png'
import high from '../assets/images/pyramid/high.png'
import extreme from '../assets/images/pyramid/extreme.png'

export const rescueSessionData = [
    {
      id: 0,
      text: 'Pick the level your anger & \nfrustration right now.'
    },
    {
      id: 1,
      text:  'Pick the level your anger & \nfrustration right now.'
    },
    {
      id: 2,
      text:  'Pick the level your anger & \nfrustration right now.'
    },
    {
      id: 3,
      text:  'Pick the level your anger & \nfrustration right now.'
    },
    {
      id: 4,
      text:  'Pick the level your anger & \nfrustration right now.'
    }
]

export const status = [
  {
    id: 0,
    status: 'Fine',
    image: fine,
  },
  {
    id: 1,
    status:  'Low',
    image: low,
  },
  {
    id: 2,
    status:  'Moderate',
    image: moderate,

  },
  {
    id: 3,
    status:  'High',
    image: high,

  },
  {
    id: 4,
    status:  'Extreme',
    image: extreme,

  }
]

export const feelingToday = [
  {
    id: 0,
    feel: ':)'
  },
  {
    id: 1,
    feel:  '>_<'
  },
  {
    id: 2,
    feel:   '-_-'
  },
  {
    id: 3,
    feel:  '0_0'
  },
  {
    id: 4,
    feel:   '>.<'
  }
]

