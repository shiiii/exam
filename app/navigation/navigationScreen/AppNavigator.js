import * as React from 'react';
import { createNativeStackNavigator,   } from '@react-navigation/native-stack';
import routes from '../routes';
import MainMenu from '../../screens/mainMenu/MainMenu'
import PyramidScale from '../../screens/rescueSession/PyramidScale'
import ScaleMeters from '../../screens/rescueSession/ScaleMeters'

const Stack = createNativeStackNavigator();

function AppNavigator() {
  return (
      <Stack.Navigator
        screenOptions={{
            headerShown: false,
        }}
      >
        <Stack.Screen name={routes.MAIN_MENU} component={MainMenu} />
        <Stack.Screen name={routes.RESCUE_SESSION} component={RescueSession} />
        <Stack.Screen name={routes.PYRAMID_SCALE} component={PyramidScale} />
        <Stack.Screen name={routes.SCALE_METERS} component={ScaleMeters} />

      </Stack.Navigator>
  );
}

export default AppNavigator;