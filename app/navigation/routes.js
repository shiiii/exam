export default Object.freeze({
    // navigator 
    APP_NAVIGATOR: 'AppNavigator',
    // main
    MAIN_MENU: 'MainMenu',

    // Rescue sessions
    RESCUE_SESSION: 'RescueSession',
    SCALE_METERS: 'ScaleMeters',
    PYRAMID_SCALE: 'PyramidScale',
    
  })
  