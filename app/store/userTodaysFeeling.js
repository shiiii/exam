import { createSelector, createSlice, createAsyncThunk } from "@reduxjs/toolkit"


const initialState = {
  feeling: null
}

const slice = createSlice({
  name: "userTodaysFeeling",
  initialState,
  reducers: {
    updateFeeling(state, { payload }) {
      console.log({path: 'update feeling', payload})
      state.feeling = payload
    },
    notificationReset: _ => initialState
  },
 
})

export const { updateFeeling, notificationReset } = slice.actions

export default slice.reducer

export const getUserTodaysFeeling = createSelector(
  state => state.userTodaysFeeling,
  (userTodaysFeeling) => userTodaysFeeling
)
