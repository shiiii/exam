import AsyncStorage from "@react-native-async-storage/async-storage"
import {
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
  } from "redux-persist"
  import { configureStore as configure } from "@reduxjs/toolkit"
import reducer from "./reducer"

const persistConfig = {
    key: "root",
    storage: AsyncStorage,
    whitelist: ['userTodaysFeeling'],
    timeout: 0,
}
  
const _persistedReducer = persistReducer(persistConfig, reducer)

export default function configureStore() {
  return configure({
    reducer: _persistedReducer,
    middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
  })
}
