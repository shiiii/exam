import React, {useState} from 'react'
import {View, StyleSheet, Dimensions, ImageBackground} from 'react-native'

import Text from '../../../components/Text'
import { colors } from '../../../config/colors'
import ProgressCircle from '../../../components/ProgressCircles'
import SliderBar from '../../../components/SliderBar'

import Circle from '../../../assets/images/circle.png'

const { width} = Dimensions.get('screen')

const SliderWidget = ({title }) => {
    const [sliderValue, setSliderValue] = useState(0)

    const onSliderValueChange = (value) => {
        setSliderValue(value * 100)
    }

    return (
        <View style = {styles.container}>
            <View style= {styles.titleContainer}  >
                <Text bold style={styles.title}>{title}</Text>
            </View>
            <View style= {styles.progressOuterContainer}  >
                <View style= {styles.progressInnerContainer}>
                    <ProgressCircle value = {sliderValue}>
                        <ImageBackground resizeMode='center' source={Circle} style={{justifyContent:'center', alignItems: 'center', height :200, width: 200,}} >
                            <Text bold style={{fontSize: 30, }}>{Math.round( sliderValue / 10)}</Text>
                        </ImageBackground>
                    </ProgressCircle>
                </View>
            </View>
            <SliderBar style={{ marginHorizontal: 15, }} onValueChange = {(value) => onSliderValueChange(value)} />
        </View>
    )
}

export default SliderWidget

const styles = StyleSheet.create({
    container: { 
        width: width, 
        backgroundColor: colors.background,
    },
    titleContainer: {
        marginBottom: 20, 
        marginHorizontal: 15 
    },
    title: {
        fontSize: 22, 
    },
    progressOuterContainer: {
        justifyContent: 'center', 
        alignItems:'center', 
        marginTop: 20, 
    },
    progressInnerContainer: { 
        justifyContent: 'center', 
        alignItems: 'center', 
        height: 300, 
        width :300, 
        borderColor: colors.gray, 
        borderWidth: 4, 
        borderRadius: 150, 
        borderStyle: 'dashed',
    }

})
