import React, {useState} from 'react'
import {View, StyleSheet, Dimensions, Image, TouchableOpacity} from 'react-native'

import Text from '../../../components/Text'
import { colors } from '../../../config/colors'

const {height, width} = Dimensions.get('screen')

const RadioButtonWidget = ({title, status,  }) => {
    const [feelingStatus, setFeelingStatus] = useState('')
    const [isSelected, setIsSelected] = useState(null)

    const handleOnSelect = (button, status) => {
        setIsSelected(button + 1)
        setFeelingStatus(status)
    }


    return (
        <View style = {styles.container}>
            <View style= {styles.titleContainer}  >
                <Text bold style={styles.title}>{title}</Text>
            </View>
            <View style={styles.statusContainer}>
                <Text bold style={styles.status}>{feelingStatus}</Text>
            </View>
            <View style = {styles.radioButtonContainer}>
                {status.map((item, index) => {
                    return (
                        <TouchableOpacity key={index} onPress={() => handleOnSelect(index, item.status)}>
                            <Image resizeMode='center'   source={item.image} style={[styles.buttonImage, { tintColor:  index >=  isSelected ? colors.gray :  colors.white}]} />
                        </TouchableOpacity>
                    )
                })}
            </View>
        </View>
    )
}

export default RadioButtonWidget

const styles = StyleSheet.create({
    container: {
        
        width: width, 
        backgroundColor: colors.background, 
    },
    titleContainer:{
        marginBottom: 20,
         marginHorizontal: 15 
    },
    title: {
        fontSize: 22, 
    },
    statusContainer: {
        alignSelf: 'center', 
        paddingVertical: 20,
    },
    status: {
        fontSize: 18
    },
    radioButtonContainer : { 
        // marginHorizontal: 50, 
        transform: [{ rotate: "180deg" }],
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonImage: {
        transform: [{ rotate: "180deg" }],
        width: 256 ,
        height: 64,
        borderColor: 'black',
    }
    
})
