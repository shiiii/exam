import React, {useState, useRef,} from 'react'
import {
    View, 
    StyleSheet, 
    ScrollView, 
    Dimensions, 
    TouchableWithoutFeedback, 
    StatusBar, 
} from 'react-native'

import Header from '../../components/Header'
import Screen from '../../components/Screen'
import { colors } from '../../config/colors'
import Button from '../../components/Button'
import { rescueSessionData, status } from '../../constant/dummyData'
import SliderWidget from './components/SliderWidget'
import StepIndicator from '../../components/StepIndicator'

const { width} = Dimensions.get('screen')

const ScaleMeters = ({}) => {
    const scrollViewRef = useRef()
    const [currentPage, setCurrentpage] = useState(1)

    const handleOnNext = () => {
        setCurrentpage(currentPage + 1)

        if (currentPage > rescueSessionData.length ) return

        if (scrollViewRef.current) {
            scrollViewRef.current.scrollTo({
                x: width * (currentPage),
                animated: true,
            })
        }
    }

    return (
        <Screen>
            <StatusBar translucent backgroundColor='transparent' />
            <TouchableWithoutFeedback onPress={handleOnNext} >
                <View style = {[styles.container, { backgroundColor: colors.background}]}>
                    <StepIndicator numberOfStep={rescueSessionData.length} current={currentPage} />
                    <Header >RESCUE SESSION : ANGER AND FRUSTRATIONS</Header>
                    <ScrollView
                        ref = { scrollViewRef}
                        horizontal
                        scrollEventThrottle={400}
                        showsHorizontalScrollIndicator = {false}
                        nestedScrollEnabled 
                        scrollEnabled = {false}
                        contentContainerStyle = {styles.scrollViewContainer}
                    > 
                        {rescueSessionData.map((item, index) => 
                            <SliderWidget key = {index} title={item.text} status = {status} />
                        )}
                    </ScrollView>
                    <View style ={styles.nextButtonContainer}>
                       { (currentPage < rescueSessionData.length) && <Button style={styles.nextButton} onPress={() => handleOnNext()}>Next</Button>}
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </Screen>
    )
}

export default ScaleMeters

const styles = StyleSheet.create({
    container: {
        flex:1,
      },
    scrollViewContainer: { 
        justifyContent: 'center', 
        marginTop: 100,
    },
    nextButtonContainer: {
        position: 'absolute',
        left: 0,
        right:0,
        bottom: 15,
        marginHorizontal: 15,
        alignItems: 'center'
    },
    nextButton: { 
        height: 40, 
        paddingHorizontal: 12,
        width: '100%', 
        borderRadius: 20,
    }
})
