import React, {useState,useEffect} from 'react'
import { View, StyleSheet} from 'react-native'
import { colors } from '../../../config/colors'
import Button from '../../../components/Button'
import Text from '../../../components/Text'

import {feelingToday} from '../../../constant/dummyData'
import Space from '../../../components/Space'
import { useDispatch, useSelector } from 'react-redux'
import {
     getUserTodaysFeeling ,
     updateFeeling,
} from '../../../store/userTodaysFeeling'

const Banner = () => {
    const dispatch = useDispatch()
    const todaysFeeling = useSelector(getUserTodaysFeeling)
    const { feeling } = todaysFeeling

    useEffect( () => {
        console.log("feeling:", todaysFeeling.feeling);
      }, [])

    const onPress = (index) =>{
        dispatch(updateFeeling(index))
    }

    return (
        <View style= {{ backgroundColor: colors.secondaryBackground, borderRadius :10, paddingHorizontal: 10, paddingTop: 20, alignItems: 'center'}}>
            <Text bold  style ={{fontSize: 18, color : colors.primary }}>
                How are you feeling today?
            </Text>
            <View style= {{ flexDirection: 'row', paddingTop: 10,}} >
                {feelingToday.map((feels, index) => {
                    return (
                        <View key= {index} >
                            <Button 
                               
                                style={[styles.button, { backgroundColor: index == todaysFeeling.feeling ? colors.babyPowder : colors.white}]}
                                onPress= {() => onPress(index)}
                            >
                                {feels.feel}
                            </Button>
                            <Space width={20}/>
                        </View>
                    )
                })}
            </View>
        </View>
    )
}

export default  Banner
const styles = StyleSheet.create({
    button: {
        height: 50,
        width: 50 ,
       margin: 10,
    },
  
  })