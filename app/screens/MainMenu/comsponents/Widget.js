import React, {useState} from 'react'
import { Image, StyleSheet, TouchableOpacity} from 'react-native'

import Text from '../../../components/Text'
import { colors } from '../../../config/colors'
import Space from '../../../components/Space'

const Widget = ({ label, icon, onPress, disabled = false }) => {
    const [itemWidth, setItemWidth] = useState(0)
  
    return (
        <TouchableOpacity disabled={disabled} style={styles.container} onPress={onPress} onLayout={(e) => setItemWidth(e.nativeEvent.layout.width)}>
          <Image source={icon} style={{ width: 25, height: 25, ...(disabled) && { tintColor: colors.medium } }} />
          <Space height={20} />
          <Text bold numberOfLines={1} style={[styles.label, { ...(disabled) && { color: colors.white }, width: itemWidth - 2 }]}>{label}</Text>
        </TouchableOpacity>
    )
  }
  
  export default Widget
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: colors.white,
      borderRadius: 10,
      paddingVertical: 15
    },
    label: {
      textAlign: 'center',
      letterSpacing: -0.6,
      fontSize: 12,
      color: colors.primary
    }
  })