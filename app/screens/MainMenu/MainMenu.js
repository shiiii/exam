import React from 'react'
import {StyleSheet, View} from 'react-native'
import Screen from '../../components/Screen'
import Text from '../../components/Text'
import Button from '../../components/Button'

import { colors } from '../../config/colors'
import Space from '../../components/Space'
import Banner from './comsponents/Banner'
import RescueSessionWidgets from './comsponents/Widget'
import Headings from '../../components/Headings'
import Widget from './comsponents/Widget'
import routes from '../../navigation/routes'

const MainMenu = ({navigation}) => {
    return (
       <Screen style={{ backgroundColor: colors.white}}>
            <View style= {{flex:1, marginHorizontal: 15, marginTop: 15,}}>
                <Banner/>
                <Space height={20} />
                <Headings header= "Rescue sessions" subHeader = "Mini sessions to help you any time" />
                <View style= {styles.sessionCintainer}>
                    <View style= {{ flexDirection: 'row', }} >
                        <Widget label="Scale Meters" onPress={() => navigation.navigate(routes.SCALE_METERS)} />
                        <Space width={20} />
                        <Widget label="Pyrammid Meters" onPress={() => navigation.navigate(routes.PYRAMID_SCALE)} />
                        {/* <Space width={20} />
                        <Widget icon={''} label="Session" onPress={() => navigation.navigate(routes.RESCUE_SESSION)} /> */}
                    </View>
                </View>
                <Space width={20} />
                <Headings header= "Rescue sessions" subHeader = "Mini sessions to help you any time" />
                <View style= {{ flexDirection: 'row', backgroundColor: colors.gray, padding: 20, borderRadius :10,}}>
                    <Widget  label="Scale Meters" onPress={() => navigation.navigate(routes.SCALE_METERS)} />
                    <Space width={20} />
                    <Widget label="Pyramid Meters" onPress={() => navigation.navigate(routes.RESCUE_SESSION)} />
                    <Space width={20} />
                    <Widget label="Scale Meters" onPress={() => navigation.navigate(routes.SCALE_METERS)} />
                </View>
            </View>
       </Screen>
    )
}

export default  MainMenu

const styles = StyleSheet.create({
    sessionCintainer: { 
        backgroundColor: colors.gray, 
        padding: 20, 
        borderRadius :10, 
    }
  })