import React from 'react'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import Routers from './app/navigation/Routers'

const App = () => {
  return (
    <SafeAreaProvider>
      <Routers/>
    </SafeAreaProvider>
  )
}

export default App